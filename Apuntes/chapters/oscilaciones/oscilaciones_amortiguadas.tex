\chapter{Oscilaciones amortiguadas}
Hasta aquí hemos considerado las oscilaciones de sistemas mecánicos cerca de un punto de equilibrio estable despreciando el impacto de las fuerzas de rozamiento.
Bajo esta simplificación la energía mecánica del sistema se conserva y si el sistema es dejado libre de oscilar la amplitud de las oscilaciones se mantiene indefinidamente en el tiempo.

En todos los sistemas reales observamos pero que si el sistema es dejado libre de oscilar la amplitud de oscilación va reduciéndose en el tiempo hasta que el sistema vuelve a estar en quiete en la posición de equilibrio.
Distintos sistemas pueden tardar más o menos tiempo en volver al estado de quiete según la importancia de las fuerzas de rozamiento pero algún mecanismo de fricción y de disipación de la energía existe en todos los sistemas mecánicos reales.

En este capítulo incluiremos, de la manera más sencilla posible, el efecto de las fuerzas de rozamiento en nuestra descripción de las oscilaciones para explicar la progresiva perdida de amplitud de las mismas. 
%
\section{Disipación de la energía y fuerzas amortiguadoras}
%
La energía es una cantidad conservada a nivel global y solo puede ser transformada entre una forma y otra.
Sin embargo la cantidad de energía mecánica almacenada en un sistema puede no conservarse por dos razones distintas.
En primer lugar, si el sistema no está perfectamente aislado de su alrededor puede ceder energía mecánica al exterior.
En segundo lugar, incluso en un sistema perfectamente aislado la energía mecánica puede ser convertida en energía térmica (calor).   
Estos dos tipos de procesos reducen la cantidad de energía mecánica del sistema a lo largo del tiempo.

La disipación de energía aviene a través de fuerzas de rozamiento o fuerzas {\it amortiguadoras}.
La característica más importante de las fuerzas amortiguadoras es que se oponen al movimiento, es decir a la velocidad, mientras las fuerzas recuperadoras se oponen al desplazamiento.

Las fuerzas amortiguadoras tienen entonces que depender de la velocidad y oponerse siempre a esta.
%
\subsection{Rozamiento seco}
%
Un ejemplo de fuerza de este tipo es la fuerza de rozamiento seco entre dos superficies en contacto.
%
\begin{figure}[htb]
\centering
\includegraphics[scale=1.5]{chapters/oscilaciones/figures/3/masa_muelle_rozamiento.pdf}
\caption{Sistema masa-muelle con rozamiento seco entre la superficie del bloque y el suelo, con coeficiente de rozamiento $\mu$.}
\label{fig:rozamiento_seco}
\end{figure}
%
Consideramos por ejemplo el sistema masa-muelle \ref{fig:masa_muelle} pero, esta vez, con una fuerza de rozamiento, caracterizada por un coeficiente de rozamiento $\mu$, entre el bloque y el suelo como mostrado en Figura \ref{fig:rozamiento_seco}.

Esta fuerza de rozamiento tendrá magnitud $|F_{\rm rs}| = \mu m g$, donde $\mu$ es el coeficiente de rozamiento, y $mg$ es la fuerza normal entre las dos superficies.
El sentido de la fuerza tiene que ser positivo cuando el bloque se mueve hacia la izquierda y negativo cuando el bloque se mueve hacia la derecha.
Este comportamiento se puede resumir con la ecuación 
%
\begin{equation}
F_{\rm rs} = -\mu m g ~\sgn(v).
\end{equation}
%
Aquí la función $\sgn(v)$ vale $1$ si $v>0$, vale $-1$ si $v<0$ y cero cuando $v=0$.
Este tipo de rozamiento puede ser interesante en algunos casos y se analiza en el Problema 16 de la colección de problemas, pero debido a la función $\sgn(v)$, que es discontinua, la ecuación de movimiento es complicada de resolver.
En este capítulo nos enfocaremos en otro tipo de fuerza de rozamiento conocida como rozamiento viscoso.
%
\subsection{Rozamiento viscoso}
%
La forma matemáticamente mas sencilla de obtener la ecuación de una fuerza que se oponga a la velocidad es usar una ley de proporcionalidad lineal con un coeficiente negativo entre la velocidad y la fuerza:
%
\begin{equation}\label{eq:fuerza_viscosa}
F_{\rm rv} = -b v,
\end{equation}
% 
donde $b$ es un numero constante positivo.

Un ejemplo de un sistema con rozamiento de este tipo es representado en Figura \ref{fig:becker}.
Un sistema masa-muelle, esta vez libre de moverse en dirección vertical, lleva una parte inmersa dentro de un vaso de precipitado que contiene agua u otro líquido viscoso (por ejemplo aceite). 
%
\begin{figure}[htb]
\centering
\includegraphics[scale=1]{chapters/oscilaciones/figures/3/masa_muelle_fluid.pdf}
\caption{Sistema masa-muelle con amortiguamiento viscoso generado por una parte que se mueve dentro de un liquido viscoso.}
\label{fig:becker}
\end{figure}
%
Si la velocidad vertical del bloque no es demasiado elevada el liquido en el vaso de precipitado se mantiene en régimen laminar sin producir turbulencia.
En estas condiciones el liquido ejerce una fuerza que se opone al movimiento directamente proporcional a la velocidad en la forma \eqref{eq:fuerza_viscosa}.   
La constante de proporcionalidad $b$ depende de la viscosidad $\eta$ del liquido y de la forma geométrica de la parte inmersa.
Por ejemplo, en el caso de una esfera de radio $R$ moviéndose a velocidad $v$ dentro de un fluido de viscosidad $\eta$, la ley de Stokes nos dice que la fuerza viscosa es
%
\begin{equation}\label{eq:stokes}
F_{\rm rv} = -6\pi R \eta~ v.
\end{equation}
%
Diferentes geometrías modificarán el coeficiente numérico pero mantendrán la proporcionalidad directa entre fuerza y velocidad. 
%
\section{Ecuación del oscilador amortiguado}
%
Consideremos ahora el sistema representado en Figura \ref{fig:masa_muelle_amortiguador} que es idéntico al sistema masa-muelle \ref{fig:masa_muelle} pero tiene un elemento más: un amortiguador ideal.
Este elemento consiste en un cilindro cerrado que contiene un pistón que se mueve dentro de un liquido viscoso.
En un amortiguador ideal la fuerza aplicada en sus extremos siempre tiene la forma \ref{eq:fuerza_viscosa}.
Podemos considerar el amortiguador ideal cómo un elemento parecido a un muelle ideal (ambos tienen masa despreciable) con la única diferencia que la fuerza proporcionada es proporcional a la velocidad de movimiento relativo de su extremos en lugar del desplazamiento.
%
\begin{figure}[htb]
\centering
\includegraphics[scale=1]{chapters/oscilaciones/figures/3/masa_muelle_amortiguador.pdf}
\caption{Sistema masa-muelle-amortiguador.}
\label{fig:masa_muelle_amortiguador}
\end{figure}
%
La ecuación de movimiento se obtiene usando la segunda ley de Newton.
El producto de la masa $m$ por el aceleración $\ddot{x}$ es igual a la suma de las fuerzas que actúan sobre el bloque.
Esta vez tenemos que considerar la fuerza elástica del muelle \eqref{eq:hooke} y la fuerza viscosa del amortiguador \eqref{eq:fuerza_viscosa}.
Sustituyendo obtenemos:
%
\begin{equation}
m \ddot{x} = -kx -b\dot{x}.
\end{equation}
%
Podemos reducir el número de parámetros independientes dividiendo por la masa.
Esto nos lleva a la siguiente forma de la ecuación 
%
\begin{equation}\label{eq:oscilador_amortiguado}
\ddot{x} +\omega_0^2 x +2\beta \dot{x}= 0,
\end{equation}
%
donde $\omega_0$ tiene la misma forma que en \eqref{eq:omega0}, y hemos definido una nueva cantidad $\beta$ que llamaremos parámetro de amortiguamiento como
%
\begin{equation}
\beta = \frac{b}{2m}.
\end{equation}
%
Notar que $\beta$ tiene dimensiones física del inverso de un tiempo, su unidad de medida es entonces, en el sistema internacional, ${\rm s^{-1}}$.
Los dos parámetros de la ecuación, $\beta$ y $\omega_0$, tienen las mismas dimensiones físicas así que es posible una comparación entre los dos.

La solución general de la ecuación \eqref{eq:oscilador_amortiguado} es conocida y depende de la relación entre los parametros $\omega_0$ y $\beta$.
En este capítulo sólo comentaremos las soluciones sin proceder a su derivación. 
Sin embargo, conociendo las soluciones, es sencillo demonstrar que respetan la ecuación diferencial \eqref{eq:oscilador_amortiguado}.

El oscilador amortiguado tiene tres regímenes de solución según la relación entre $\omega_0$ y $\beta$.
Las oscilaciones se dicen inframortiguadas si $\omega_0>\beta$, amortiguadas críticamente si $\omega_0=\beta$ y sobre-amortiguadas si $\omega_0<\beta$.
Describiremos ahora en el detalle estos tres tipos de oscilaciones amortiguadas.     
%
\subsection{Oscilaciones inframortiguadas \texorpdfstring{($\omega_0 > \beta$)}{}}
%
La solución de la ecuación \eqref{eq:oscilador_amortiguado} en el régimen infra-amortiguado ($\omega_0 > \beta$) es  
%
\begin{equation}\label{eq_posicion_infra}
x(t) = Ae^{-\beta t}\cos(\Omega t +\phi),
\end{equation}
%
donde la frecuencia angular $\Omega$ es 
%
\begin{equation}\label{eq:omega_amortiguadas}
\Omega = \sqrt{\omega_0^2-\beta^2}.
\end{equation}
%
Esta solución se reduce a la solución del oscilador sin amortiguamiento si $\beta=0$.
Si la comparamos con la solución del oscilador armónico simple \eqref{eq:MAS} notamos que la presencia del amortiguamiento introduce dos efectos distintos:
%
\begin{itemize}
\item La amplitud de las oscilaciones $A$, que en ausencia de amortiguamiento es constante, cambia a una función exponencial decreciente $A(t) = Ae^{-\beta t}$.
\item La frecuencia angular de las oscilaciones se reduce con respeto al valor $\omega_0$ en ausencia de amortiguamiento al valor $\Omega$ definido en \eqref{eq:omega_amortiguadas} 
\end{itemize}
%
La figura \ref{fig:underdamped} muestra un ejemplo de gráfica de la posición y de la velocidad de un oscilador infra-amortiguado.
La posición (y la velocidad) oscila entre valores positivos y negativos cruzando la posición de equilibrio $x=0$ a intervalos regulares.
El movimiento no es periódico ya que cada oscilación tiene una amplitud menor que la precedente. 
Sin embargo tiene sentido definir un ``período'' $T=2\pi/\Omega$ que representa el tiempo entre dos máximos sucesivos (o entre dos mínimos sucesivos). 
La amplitud de las oscilaciones tarda un tiempo 
%
\begin{equation}
\tau_A = \frac{1}{\beta},
\end{equation} 
%
en reducirse de un factor $e^{-1}$ comparado con el valor inicial.
Este tiempo se llama {\it tiempo de extinción de la amplitud} y nos da una medida de canto tiempo tarda el movimiento en apagarse.
%   
\begin{figure}[htb]
\centering
\includegraphics[scale=.9]{chapters/oscilaciones/figures/3/position_underdamped.pdf}
\includegraphics[scale=.9]{chapters/oscilaciones/figures/3/velocity_underdamped.pdf}
\caption{Posición y velocidad de un sistema oscilante infra-amortiguado con $k=1~{\rm N/m}$, $m=1~{\rm kg}$, $b=0.1~{\rm kg/s}$. El sistema empieza en $t=0$ con $x(0)=1~{\rm m}$ y $v(0) = 0~{\rm m/s}$}
\label{fig:underdamped}
\end{figure}
%
\subsubsection{Energía de las oscilaciones inframortiguadas}
%
En presencia de amortiguamiento la energía del movimiento se reduce progresivamente.
Para calcular como evoluciona la energía del sistema de forma cuantitativa necesitamos la expresión de la posición \eqref{eq_posicion_infra} y su derivada, la velocidad:
%
\begin{equation}
v(t) = -Ae^{-\beta t}[\beta\cos(\Omega t +\phi)+\Omega \sin(\Omega t +\phi)].
\end{equation}
%
Recordando que la energía mecánica total es la suma de energía potencial del muelle y energía cinética del bloque obtenemos:
%
\begin{equation}\label{eq:energiainfra}
\begin{split}
E(t) & = \frac{1}{2}kx^2 (t) + \frac{1}{2}mv^2 (t)
= \frac{1}{2}k A^2e^{-2\beta t}\cos^2(\Omega t +\phi)+ \frac{1}{2}m A^2e^{-2\beta t}[\beta\cos(\Omega t +\phi)+\Omega \sin(\Omega t +\phi)]^2\\
& = \frac{1}{2}A^2e^{-2\beta t}\{k\cos^2(\Omega t +\phi)+ m[\beta\cos(\Omega t +\phi)+\Omega \sin(\Omega t +\phi)]^2\}\\
& = \frac{1}{2}mA^2e^{-2\beta t}\{(\omega_0^2+\beta^2)\cos^2(\Omega t +\phi)+\Omega^2 \sin^2(\Omega t +\phi)+2\beta \Omega \sin(\Omega t +\phi)\cos(\Omega t +\phi)\}.
\end{split}
\end{equation}
%
Antes de seguir con el desarrollo completo definimos una cantidad que llamamos {\it energía mecánica media} que corresponde a la media en un período de las oscilaciones de la energía mecánica y que indicaremos con $\bar{E}(t)$.
La energía mecánica media se obtiene sustituyendo las funciones trigonométricas en la ultima línea de \eqref{eq:energiainfra} con sus valores medios en un período:
%
\begin{equation}\label{eq:energia_media}
\bar{E}(t) = \frac{1}{2}mA^2e^{-2\beta t}\{(\omega_0^2+\beta^2)\cdot\frac{1}{2}+\Omega^2\cdot\frac{1}{2} +2\beta \Omega \cdot0\} = \frac{1}{2}kA^2e^{-2\beta t}.
\end{equation}
% 
Los valores medios en en período de $\cos^2(x)=[1+\cos(2x)]/2$ y $\sin^2(x)=[1-\cos(2x)]/2$ valen $1/2$ mientras que el valor medio de $\sin(x)\cos(x)=1/2\sin(2x)$ es cero.  
La ecuación \eqref{eq:energia_media} es mucho más manejable que la expresión completa de la energía y coincide con \eqref{eq:energia_MAS} si sustituimos la constante $A$ con una amplitud que cambia en el tiempo $A(t)=Ae^{-\beta t}$.

Retomando el desarrollo de la energía mecánica exacta \eqref{eq:energiainfra} podemos demostrar que $E(t)$ es igual a $\bar{E}(t)$ más unas oscilaciones con frecuencia doble de la frecuencia de las oscilaciones:
%
\begin{equation}
\begin{split}
E(t)& = \frac{1}{2}mA^2e^{-2\beta t}\{\frac{\omega_0^2+\beta^2+\Omega^2}{2}
+\frac{\omega_0^2+\beta^2-\Omega^2}{2}\cos(2\Omega t +2\phi)
+\beta\Omega\sin(2\Omega t +2\phi)\}\\
& = \frac{1}{2}mA^2e^{-2\beta t}\{\omega_0^2
+\beta^2\cos(2\Omega t +2\phi)
+\beta\Omega\sin(2\Omega t +2\phi)\}\\
& = \bar{E}(t)\{1
+\frac{\beta^2}{\omega_0^2}\cos(2\Omega t +2\phi)
+\frac{\beta\Omega}{\omega_0^2}\sin(2\Omega t +2\phi)\}\\
& = \bar{E}(t)\{1+\frac{\beta}{\omega_0}[
\frac{\beta}{\omega_0}\cos(2\Omega t +2\phi)
+\frac{\Omega}{\omega_0}\sin(2\Omega t +2\phi)]\}\\
& = \bar{E}(t)\{1+\frac{\beta}{\omega_0}[
\sin(\psi)\cos(2\Omega t +2\phi)
+\cos(\psi)\sin(2\Omega t +2\phi)]\}\\
& = \bar{E}(t)\{1+\frac{\beta}{\omega_0}\sin(2\Omega t +2\phi+\psi)\}.\\
\end{split}
\end{equation}
%
En esta demostración hemos usado las fórmulas trigonométricas $\cos^2(x)=[1+\cos(2x)]/2$, $\sin^2(x)=[1-\cos(2x)]/2$, y $\sin(x)\cos(x)=1/2\sin(2x)$. 
Además, en la quinta línea, hemos definido $\psi$ como aquél ángulo cuyo seno es $\beta/\omega_0$ y su coseno $\Omega/\omega_0$ (notar que la suma de los dos cuadrados es uno).
El resultado se muestra gráficamente en Fig.~\ref{fig:energy_underdamped} donde está representada la energía total de un movimiento oscilatorio inframortiguado.
% 
\begin{figure}
\centering
\includegraphics[scale=.9]{chapters/oscilaciones/figures/3/energy_underdamped.pdf}
\caption{Energía del movimiento oscilatorio representado en Fig.~\ref{fig:underdamped}. La figura muestra por separado la contribución de la energía potencial (azul) y de la energía cinética (naranja) a la energía mecánica total $E(T)$ (verde). La línea negra es la energía media $\bar{E}(t)$.
\label{fig:energy_underdamped}}
\end{figure}
%
Como las oscilaciones al rededor del valor medio son muy rápidas y pequeñas si el amortiguamiento es pequeño usaremos la energía media como medida de la energía mecánica del sistema.
A partir de ahora nos referiremos a $\bar{E}(t)$ como energía del sistema y dejaremos de usar la notación $\bar{E}(t)$ sustituyéndola simplemente con $E(t)$.
Así podemos decir que la energía del movimiento inframortiguado se disipa exponencialmente
%
\begin{equation}\label{eq:energia_decaimiento}
E(t) = E_0e^{-2\beta t}=E_0e^{-t/\tau_E}.
\end{equation}
%
Notamos que la constante que aparece en el exponente es $2\beta$ en lugar del $\beta$ que aparece en la amplitud.
Eso quiere decir que la energía decae el doble de rápido que la amplitud.
El mismo decaimiento exponencial se puede representar con un tiempo característico 
%
\begin{equation}
\tau_E = \frac{1}{2\beta}
\end{equation}
%
que toma el nombre de {\it tiempo de extinción de la energía} y representa el tiempo que tarda la energía en reducirse de un factor $1/e$ respecto del valor inicial.
\subsection{Amortiguamiento crítico \texorpdfstring{($\omega_0 = \beta$)}{}}
%
La condición de amortiguamiento crítico corresponde a valores de los parámetros $\omega_0$ y $\beta$ numéricamente iguales entre ellos.
En un sistema real los dos parámetros no son nunca exactamente iguales (siempre hay cierta incertidumbre) y la condición de igualdad perfecta es sólo una abstracción matemática.
Sin embargo analizar el caso en que los dos parámetros coinciden exactamente nos da una guia para entender los casos reales en que $\omega_0$ y $\beta$ toman valores muy parecidos.
En este régimen la solución de la ecuación diferencial \eqref{eq:oscilador_amortiguado} es 
%
\begin{equation}\label{eq:x_critico}
x(t) = a e^{-\beta t} + b t e^{-\beta t}=e^{-\beta t}(a+bt),
\end{equation}
%
donde $a$ y $b$ son constantes que dependen de las condiciones iniciales.
La velocidad asociada a esta solución es:
%
\begin{equation}
v(t) =  a (-\beta) e^{-\beta t} + b (1-\beta t) e^{-\beta t}=e^{-\beta t}[-\beta a+b(1-\beta t)],
\end{equation}
%
mientras que la aceleración vale
%
\begin{equation}
a(t) = e^{-\beta t} [  a \beta^2 +b (\beta^2t-2\beta)].
\end{equation}
%
Sustituyendo en \eqref{eq:oscilador_amortiguado} podemos demostrar que la solución es válida.

Las constantes $a$ y $b$ se pueden expresar en función de la posición y de la velocidad en el instante $t=0$ ($x_0$ y $v_0$ respectivamente):
\begin{align}
x(0) =a=x_0\\
v(0) = -\beta a+b=v_0
\end{align}
resolviendo el sistema y sustituyendo en \eqref{eq:x_critico} obtenemos:
%
\begin{equation}
x(t) = x_0 e^{-\beta t} + (\beta x_0 +v_0) t e^{-\beta t}.
\end{equation}
%
Notamos que esta solución (o la \eqref{eq:x_critico} obviamente) no involucra ninguna función oscilante y se compone sólo de un exponencial y un polinomio. Por lo tanto el movimiento no tiene la característica periodicidad del movimiento armónico simple o la ``casi periodicidad'' de las oscilaciones infra amortiguadas.
%
\begin{figure}[!!hbt]
\centering
\includegraphics[scale=.9]{chapters/oscilaciones/figures/3/position_critical.pdf}
\caption{Posición y velocidad de un sistema oscilante críticamente amortiguado con $k=1~{\rm N/m}$, $m=1~{\rm kg}$, $b=2~{\rm kg/s}$. El sistema empieza en $t=0$ con $x(0)=1~{\rm m}$ y distintos valores de $v(0)$.}
\label{fig:critical}
\end{figure}
%
En figura \ref{fig:critical} podemos ver ejemplos de la trayectoria de un movimiento críticamente amortiguado  por distintas condiciones iniciales.
El sistema vuelve al equilibrio sin hacer ``verdaderas oscilaciones'' al rededor de la posición de equilibrio y la cruza, como máximo, una vez durante el movimiento.

El aspecto más importante del oscilador armónico con amortiguamiento crítico es que es el tipo de oscilador amortiguado que vuelve a la posición de equilibrio sin oscilar y en el menor tiempo posible.
Eso tiene aplicación en todos los sistemas que queremos diseñar de manera que vuelvan rápidamente al equilibrio después de haber sufrido una perturbación, como por ejemplo las suspensiones de los vehículos.
%
\subsection{Oscilaciones sobre amortiguadas \texorpdfstring{($\omega_0 < \beta$)}{}}
%
En el caso de sobreamortiguamiento ($\beta>\omega_0$) la solución de la ecuación diferencial del oscilador armónico amortiguado \eqref{eq:oscilador_amortiguado} es:
%
\begin{equation}\label{eq:x_sobre}
x(t) = a e^{-(\beta+\gamma) t} + b  e^{-(\beta-\gamma) t},
\end{equation}
%
donde
%
\begin{equation}
\gamma = \sqrt{\beta^2-\omega_0^2}<\beta,
\end{equation}
%
y $a$ y $b$ son constantes que se determinan a partir de las condiciones iniciales.
Tampoco esta solución involucra funciones trigonométricas, es simplemente la combinación lineal de dos exponenciales que se acercan asíntoticamente a cero.
%
\begin{figure}[hbt]
\centering
\includegraphics[scale=.9]{chapters/oscilaciones/figures/3/position_overdamped.pdf}
\caption{Posición y velocidad de un sistema oscilante sobre-amortiguado con $k=1~{\rm N/m}$, $m=1~{\rm kg}$, $b=4~{\rm kg/s}$. El sistema empieza en $t=0$ con $x(0)=1~{\rm m}$ y distintos valores de $v(0)$.}
\label{fig:overdamped}
\end{figure}
%
En figura \ref{fig:overdamped} podemos ver distintas trayectorias de un sistema sobre amortiguado. 
También en este caso las trayectorias cruzan la posición de equilibrio como máximo una vez y luego vuelven al equilibrio.
En este caso lo hacen más lentamente que en el caso de amortiguamiento crítico.

La velocidad asociada al movimiento es:
%
\begin{equation}
v(t) =  -(\beta+\gamma)a e^{-(\beta+\gamma) t} -(\beta-\gamma) b  e^{-(\beta-\gamma) t},
\end{equation}
%
mientras que la aceleración vale 
%
\begin{equation}
a(t) =  (\beta+\gamma)^2a e^{-(\beta+\gamma) t} +(\beta-\gamma)^2 b  e^{-(\beta-\gamma) t}.
\end{equation}
%
Sustituyendo estas expresiones en \eqref{eq:oscilador_amortiguado} podemos averiguar que la solución \eqref{eq:x_sobre} es correcta.

Podemos expresar las constantes $a$ y $b$ usando la posición $x_0$ al tiempo $t=0$ y la correspondiente velocidad $v_0$:
%
\begin{align}
x(0) =a+b=x_0\\
v(0) = -\beta (a+b)+\gamma(b-a)=v_0.
\end{align}
%
Resolviendo este sistema y sustituyendo en \eqref{eq:x_sobre} obtenemos:
%
\begin{equation}
x(t) = \frac{(\gamma-\beta)x_0 -v_0}{2\gamma} e^{-(\beta+\gamma) t} + \frac{(\gamma+\beta)x_0 +v_0}{2\gamma}  e^{-(\beta-\gamma) t}.
\end{equation}
%
%
\subsection{Factor de calidad}
\label{sec:calidad}
%
La ecuación del oscilador armónico amortiguado \eqref{eq:oscilador_amortiguado} tiene dos parámetros $\omega_0$ y $\beta$ con las mismas dimensiones físicas: el reciproco de un tiempo.
El comportamiento cualitativo de las soluciones (si el movimiento es inframortiguado, sobreamortiguado o críticamente amortiguado) depende de la relación entre los dos parámetros.
Por esta razón es útil definir un nuevo parámetro numérico {\it sin dimensiones físicas} que nos permita comparar las propiedades de osciladores que tienen frecuencias naturales completamente distintas.
Este nuevo parámetro se llama {\it factor de calidad} de un oscilador armónico amortiguado y su definición es 
%
\begin{equation}\label{eq:q_definicion}
Q=\frac{\omega_0}{2\beta}.
\end{equation}
%
Con esta definición podemos volver a escribir la ecuación del oscilador armónico usando $Q$ en lugar de $\beta$ como parámetro que representa el amortiguamiento:
%
\begin{equation}
\ddot{x} +\frac{\omega_0}{Q}\dot{x} +\omega_0^2 x =0.
\end{equation}
%
Esta forma permite evidenciar que el parámetro dimensional $\omega_0$ fija la escala de tiempo en que la solución cambia (determina el período de las oscilaciones) mientras que el parámetro adimensional $Q$ determina su forma como podemos ver en Fig.~\ref{fig:different_Q}. 
%
\begin{figure}
\centering
\includegraphics[scale=.9]{chapters/oscilaciones/figures/3/position_Q_dimensionless.pdf}
\includegraphics[scale=.9]{chapters/oscilaciones/figures/3/position_Q.pdf}
\caption{Izquierda: diferentes tipos de movimiento oscilatorio con las mismas condiciones iniciales representados en función de $\omega_0 t$. Movimiento sobreamortiguado (azul), movimiento críticamente amortiguado (naranja), movimiento inframortiguado (verde), movimiento armónico simple (rojo). 
Derecha: comparación de oscilaciones infra amortiguadas ($k=1~{\rm N/m}$, $m=1~{\rm kg}$) con diferentes factores de calidad (diferentes valores de $b$): $Q=2$ (azul) $Q=5$ (naranja) $Q=50$ (verde).}
\label{fig:different_Q}
\end{figure}
%  
Evidentemente, de la definición \eqref{eq:q_definicion} sigue que la clasificación de las soluciones según el valor de $Q$ es:
%
\begin{itemize}
\item Movimiento armónico simple si $Q=+\infty$.
\item Oscilaciones inframortiguadas si $Q>1/2$, el amortiguamiento es débil si $Q\gg 1$.
\item Oscilaciones críticamente amortiguadas si $Q=1/2$.
\item Oscilaciones sobreamortiguadas si $0<Q<1/2$.
\end{itemize}
%
El concepto de factor de calidad es especialmente útil en el caso de oscilaciones inframortiguadas ($Q>1/2$), y especialmente cuando el amortiguamiento es débil ($Q\gg 1$).

En el caso de oscilaciones con amortiguamiento débil ($\beta \ll \omega_0$, $Q\gg 1$) podemos establecer una relación entre el factor de calidad $Q$ y el número de oscilaciones que el sistema hace antes de que su amplitud se reduzca significativamente.

Recordando que la amplitud de las oscilaciones inframortiguadas decae como
%
\begin{equation}
A(t) = A_0e^{-\beta t}
\end{equation}
% 
la amplitud después de $Q$ oscilaciones será 
%
\begin{equation}
A(QT) = A_0e^{-\beta QT} =  A_0\exp\left(-\beta \frac{\omega_0}{2\beta}\frac{2\pi}{\Omega}\right)\approx A_0e^{-\pi} \approx  0.0432 A_0 = 4.32\% A_0.
\end{equation}
%
En la derivación hemos despreciado la diferencia entre $\Omega$ la frecuencia de las oscilaciones amortiguadas y la frecuencia natural del sistema $\omega_0$ ya que esta es pequeña en el caso de amortiguamiento débil.

El factor de calidad $Q$ nos da entonces una medida del número de oscilaciones que tarda el sistema en reducir su amplitud de oscilación a un $\approx 4\%$ del valor inicial.
Contando el número de oscilaciones que se pueden distinguir fácilmente en una figura (sin alargarla) da una estimación del factor de calidad como se puede ver en figura \ref{fig:different_Q}. 

Siempre en el caso de oscilaciones con amortiguamiento débil podemos relacionar $Q$ con la fracción de la energía que se disipa en cada oscilación. 
Recordando que la energía decae según la ley exponencial \eqref{eq:energia_decaimiento} podemos calcular la pérdida relativa de energía en una oscilación
%
\begin{equation}
\frac{E(t)-E(t+T)}{E(t)}=\frac{E_0e^{-2\beta t}-E_0e^{-2\beta (t+T)}}{E_0e^{-2\beta t}}=1-e^{-2\beta T} \approx 1-e^{-2\beta 2\pi/\omega_0}=1-e^{-\frac{2\pi}{Q}}\approx \frac{2\pi}{Q}.
\end{equation}
%
En esta derivación hemos vuelto a despreciar la diferencia entre $\Omega$ y $\omega_0$ y hemos usado la aproximación en serie de Taylor del exponencial $e^x\approx 1+x$ (válida cuando $x\ll 1$).

El factor de calidad es entonces
%
\begin{equation}
Q\approx 2\pi \frac{E(t)}{E(t)-E(t+T)} =2\pi \frac{\mbox{energía del oscilador}}{\mbox{energía disipada en una osilción}}.
\end{equation}
%
Algunos autores usan esta ecuación para definir e factor de calidad. Las dos definiciones coinciden en el régimen de amortiguamiento débil.

Según el tipo de aplicación puede ser deseable reducir o augmentar el factor de calidad de un oscilador.

Cuando las oscilaciones son vibraciones indeseadas se intenta normalmente reducir su factor de calidad.
Algunos edificios especialmente altos (Por ejemplo el rascacielos Taipei 101 en Taipei (Taiwan)) tienen un sistema de absorción de las vibraciones basado en una gran masa colgada de un sistema de muelles y amortiguadores que ayuda a reducir el factor de calidad de las oscilaciones del edificio.

Al revés, cuando el sistema considerado está diseñado específicamente como oscilador, cómo en el caso de un reloj, es normalmente deseable maximizar el factor de calidad.
En el caso de los relojes un mayor factor de calidad del oscilador está asociado con una mayor precisión en la medida del tiempo y la evolución de los relojes está relacionada directamente con la capacidad de construir osciladores con factores de calidad más altos.

Por ejemplo un reloj de péndulo domestico de buena calidad puede llegar a tener un factor de calidad $Q\approx 10 000$ y un error de $\approx 10$ segundos al mes, siendo mucho más preciso que un reloj mecánico de pulsera que alcanza como máximo $Q\approx 100-300$.
El reloj de péndulo comercial más preciso jamás construido (el reloj Shortt), cuyo péndulo oscila en una cámara de vacío para eliminar la fricción del aire, tenía un factor de calidad $Q\approx 110 000$, produciendo un error de tan solo $1$ segundo por año.
Los relojes de péndulo fueron sustituidos en los años $1930$ por osciladores de cuarzo que alcanzan fácilmente factores de calidad $Q\approx 10^5-10^6$.      