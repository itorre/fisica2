\chapter{Pequeñas oscilaciones de sistemas con un grado de libertad}
%
En este capítulo analizaremos las oscilaciones de varios sistemas mecánicos cuando su equilibrio es perturbado por una perturbación inicial pequeña.

Demostraremos que una clase muy importante de sistemas (casi todos los sistemas cerca de una posición de equilibrio estable) oscila con un movimiento armónico simple si la amplitud de las oscilaciones es suficientemente pequeña.
%
\section{Oscilaciones de sistemas en rotación}
%
En el capítulo anterior obtuvimos y resolvimos la ecuación de un sistema que hace un movimiento oscilatorio de traslación en una dimensión del espacio. 
En esta sección analizaremos en detalle algunos sistemas que están vinculados a hacer un movimiento de rotación al rededor de un eje fijo para encontrar las condiciones en que este movimiento es oscilatorio y en particular armónico simple.  
%
\subsection{El péndulo simple}
%
El prototipo de sistema que hace un movimiento de rotación oscilatorio es el péndulo simple.
Está constituido por una varilla de longitud $\ell$ y masa despreciable vinculada a girar al rededor de un punto fijo, y con una masa puntual $m$ enganchada al otro extremo como mostrado en figura \ref{fig:pendulo_dynamics}.
%  
\begin{figure}[htb]
\centering
\includegraphics[scale=1]{chapters/oscilaciones/figures/2/pendulum_forces.pdf}
\caption{Péndulo simple con indicadas las dimensiones geométricas relevantes. En rojo la fuerza peso.}
\label{fig:pendulo_dynamics}
\end{figure}
%
El estado del sistema está completamente descrito por la coordenada angular $\theta$ (el ángulo entre la varilla y la vertical) y por la velocidad angular $d\theta /dt=\dot{\theta}$ (a partir de ahora usaremos la notación $\dot{f}$ para la derivada temporal de la cantidad $f$).

Aplicando el segundo principio de la dinámica para el movimiento de rotación
%
\begin{equation}\label{eq:movimiento_angular}
I \alpha = I \ddot{\theta} = M,
\end{equation}
%
donde $I=m\ell^2$ es el momento de inercia del péndulo con respecto al eje de rotación, $\alpha$ es la aceleración angular (la derivada segunda de $\theta$) y $M$ es el momento total de las fuerzas que actúan sobre el péndulo (otra vez medido con respecto al centro de rotación).

Sobre el péndulo, considerado como un conjunto, actúan dos fuerzas: el peso y la normal ejercida por la clavija.
La normal no produce momento con respecto al centro de rotación ya que está aplicada en el mismo centro.
El peso $-mg\hat{z}$ es responsable por el momento total que vale
%
\begin{equation}\label{eq:momento_pendulo}
M = -mg \ell \sin(\theta).
\end{equation}
%
Sustituyendo \eqref{eq:momento_pendulo} y el valor del momento de inercia en \eqref{eq:movimiento_angular} obtenemos
%
\begin{equation}\label{eq:pendulum}
\frac{d^2\theta}{dt^2} = -\omega_0^2 \sin(\theta),
\end{equation}
%
donde
%
\begin{equation}\label{eq:omega_pendulo}
\omega_0 =\sqrt{\frac{mg\ell}{m\ell^2}}= \sqrt{\frac{g}{\ell}}.
\end{equation}
%
Notamos que en este caso, a diferencia del sistema masa-muelle, la fuerza recuperadora es proporcionada por la gravedad.
En consecuencia de eso la masa juega aquí un doble papel.
Por un lado contribuye a la inercia del sistema a través del momento de inercia $I=m\ell^2$ y por el otro determina la fuerza peso $mg$ que actúa de fuerza recuperadora.
La masa aparece entonces tanto en el numerador como en el denominador de \eqref{eq:omega_pendulo} y se cancela de la expresión para la frecuencia angular $\omega_0$.

La ecuación \eqref{eq:pendulum} es la ecuación diferencial del movimiento de un péndulo simple, se puede resolver  analíticamente (pero la solución involucra las funciones elípticas) o numéricamente a partir de ciertas condiciones iniciales (ángulo y velocidad angular en un instante inicial).
En nuestra análisis nos limitaremos a distinguir los regímenes del movimiento descrito por \eqref{eq:pendulum} y resolver la ecuación del movimiento sólo en el caso de pequeñas oscilaciones, cuando la solución es mucho más sencilla. 
%
\subsubsection{Energía del péndulo simple}
%
Para entender de manera cualitativa los distintos regímenes del movimiento del péndulo simple es útil razonar utilizando la energía.
Las dos energías involucradas en el movimiento de este sistema son la energía cinética 
%
\begin{equation}
K = \frac{1}{2} mv^2=\frac{1}{2} I\dot{\theta}^2=\frac{1}{2} m\ell^2 \dot{\theta}^2,
\end{equation}
%
y la energía potencial gravitacional del péndulo
%
\begin{equation}\label{eq:potential_pendulum}
U =mgz= mg\ell [1-\cos(\theta)],
\end{equation}
%
donde hemos fijado el origen de la coordenada $z$ en la posición de equilibrio estable del péndulo ($\theta = 0$).
La suma de estas dos energías es la energía mecánica $E$ que es una constante del movimiento y puede ser calculada en cualquier punto de la trayectoria ya que su valor se mantiene constante en el tiempo.
En particular es posible calcular el valor de la energía en el instante inicial del movimiento
%
\begin{equation}
E = \frac{1}{2} m\ell^2 \dot{\theta}^2 + mg\ell [1-\cos(\theta)] = \frac{1}{2} m\ell^2 \dot{\theta_0}^2 + mg\ell [1-\cos(\theta_0)] ,
\end{equation}
%
donde $\theta_0$ y $\dot{\theta}_0$ son el ángulo y la velocidad angular iniciales.
Utilizando la conservación de la energía $dE/dt=0$ es posible, siguiendo los mismos pasos que en el capítulo anterior, encontrar la ecuación del movimiento \eqref{eq:pendulum}.
%
\subsubsection{Regímenes de movimiento del péndulo simple}
%
La figura \ref{fig:pendulo_energy} representa la energía potencial del péndulo \eqref{eq:potential_pendulum}.
La gráfica de la energía potencial está comprendida entre el valor mínimo cero (correspondiente a la posición de equilibrio estable $\theta=0$) y el valor máximo $2mg\ell$ que se alcanza en la posición vertical de equilibrio inestable $\theta=\pm \pi$. 
%  
\begin{figure}[htb]
\centering
\includegraphics[scale=1]{chapters/oscilaciones/figures/2/energia_pendulo.pdf}
\caption{Energía potencial de un péndulo en función del ángulo de inclinación (azul).
Las líneas verdes representan la energía total en tres casos distintos: $E > 2mg\ell$ (línea de guiones y puntos), $0<E < 2mg\ell$ (línea discontinua), y $0<E \ll 2mg\ell$ (línea continua). 
Los puntos verdes marcan los ángulos máximos alcanzados.
La línea discontinua azul es la aproximación parabólica de la energía potencial.}
\label{fig:pendulo_energy}
\end{figure}
%
Según cual sea el valor de la energía mecánica total, comparado con el valor máximo de la energía potencial $2mg\ell$, se pueden dar tres posibilidades.  
%
\begin{itemize}
\item Si $E > 2mg\ell$ el péndulo tiene energía suficiente para superar el máximo de la energía potencial y la trayectoria será un movimiento circular (no uniforme) con $\theta$ que crece de manera monótona. 
\item Si $0<E < 2mg\ell$ el péndulo no tiene energía suficiente para superar el máximo de la energía potencial.
La trayectoria estará limitada entre $\pm \theta_{\rm max}$, donde el ángulo máximo alcanzado se encuentra igualando la energía total a la energía potencial, $\theta_{\rm max} = \arccos[1-E/(mg\ell)]$.
El movimiento será un {\it movimiento oscilatorio} entre $\pm \theta_{\rm max}$ pero no será en general un movimiento armónico simple. 
\item Si $0<E \ll 2mg\ell$ el ángulo máximo será muy pequeño ($\theta \ll 1$ si medido en radianes).
En este caso, como veremos enseguida las oscilaciones serán un movimiento armónico simple. 
\end{itemize}
%
\subsubsection{Pequeñas oscilaciones del péndulo simple}
%
En el tercer caso, cuando el ángulo máximo de oscilación es pequeño podemos simplificar la ecuación del movimiento aproximando la función seno en el miembro de derecha.
La función seno (se entiende que el angulo se mide en radianes) para ángulos pequeños se puede aproximar con su primer orden en serie de Taylor
%
\begin{equation}
\sin(x)\approx x.
\end{equation}
%
Con esta aproximación \eqref{eq:pendulum} se transforma en una ecuación diferencial lineal mucho más sencilla:
%
\begin{equation}\label{eq:pendulo_armonico}
\frac{d^2\theta}{dt^2} \approx -\omega_0^2 \theta.
\end{equation}
%
En el mismo rango de ángulos la energía potencial \eqref{eq:potential_pendulum} se puede aproximar usando la expansión de Taylor del coseno $\cos(\theta)\approx 1-\theta^2/2$ obteniendo
%
\begin{equation}\label{eq:potential_pendulum2}
U \approx \frac{1}{2}mg\ell\theta^2,
\end{equation}
%
Que tiene la misma forma cuadrática que la energía potencial de un muelle ideal.

La ecuación \eqref{eq:pendulo_armonico} es formalmente idéntica a la ecuación del oscilador armónico y tiene entonces la misma solución
%
\begin{equation}
\theta(t) = A \cos(\omega_0 t+ \phi),
\end{equation}
%
dónde $A$ y $\phi$ son dos constantes que hay que determinar a partir de las condiciones iniciales.
%
\subsection{El péndulo físico} 
%
El péndulo físico es un cuerpo de forma arbitraria y masa total $m$ que está vinculado a moverse al rededor de un eje fijo como mostrado en Figura \ref{fig:pendulo_fisico}.
El movimiento del péndulo físico se rige por las mismas ecuaciones que describen el movimiento del péndulo simple y tiene por consecuencia, los mismos regímenes de movimiento y las mismas soluciones para la trayectoria. 
Sólo cabe recordar dos diferencias.
%
\begin{figure}[htb]
\centering
\includegraphics[scale=1]{chapters/oscilaciones/figures/2/pendulo_fisico.pdf}
\caption{Péndulo físico con indicadas las dimensiones geométricas relevantes. En rojo la fuerza peso.}
\label{fig:pendulo_fisico}
\end{figure}
%

Primero, la magnitud del momento de la fuerza peso está determinada por la distancia $D$ entre el centro de rotación y el centro de masa en lugar de la longitud total.
El momento total de las fuerzas es entonces
%
\begin{equation}
M = -mg D \sin(\theta).
\end{equation}
%
Segundo, el momento de inercia $I$ que aparece en la ecuación del movimiento angular \eqref{eq:movimiento_angular} no es simplemente $m\ell^2$ sino tiene que calcularse teniendo en cuenta la forma del cuerpo y su distribución de masa.

La ecuación del movimiento del péndulo físico es idéntica a \eqref{eq:pendulum} donde la frecuencia angular natural es
%
\begin{equation}\label{eq:frequencia_angular_pendulo_fisico}
\omega_0=\sqrt{\frac{mgD}{I}}.
\end{equation}
%
Notamos que $I$ es el momento de inercia con respecto al centro de rotación. 
A menudo puede resultar más sencillo expresar el momento de inercia $I$ usando el momento de inercia con respecto al centro de masa $I_{\rm cm}$.
Esto se puede hacer utilizando el teorema del eje paralelo (o teorema de Steiner) que relaciona los dos momentos de inercia
%
\begin{equation}
I = I_{\rm cm}+mD^2.
\end{equation}
%
Sustituyendo en \eqref{eq:frequencia_angular_pendulo_fisico} se obtiene 
%
\begin{equation}\label{eq:frequencia_angular_pendulo_fisico2}
\omega_0=\sqrt{\frac{mgD}{I_{\rm cm}+mD^2}}.
\end{equation}
%
%
\subsubsection{Un ejemplo de péndulo físico}
%
Para entender mejor como calcular la frecuencia natural de un péndulo físico resolvemos el siguiente ejemplo. 
%
\begin{figure}[htb]
\centering
\includegraphics[scale=1]{chapters/oscilaciones/figures/2/codo.pdf}
\includegraphics[scale=1]{chapters/oscilaciones/figures/2/codo2.pdf}
\caption{Izquierda: dimensiones geométricas del péndulo físico considerado.
Derecha: posiciones de los centros de masa de los dos segmentos y del objeto total.}
\label{fig:codo}
\end{figure}
%

Un alambre homogéneo de masa $m$ y longitud $L$ se dobla por su punto medio hasta que sus dos mitades formen un codo de $60^\circ$ (Ver Figura \ref{fig:codo}). 
Si dicho codo se coloca ahora sobre un eje horizontal y dejamos que efectúe pequeñas oscilaciones, de forma que el plano del codo se mantenga en todo momento vertical, ¿cuánto valdrá el período $T$ de dicho movimiento?

Para resolver este problema necesitamos conocer la distancia $D$ entre el centro de masa y el punto de suspensión y el momento de inercia $I$ del alambre doblado.
El cuerpo se puede considerar como la unión de dos varillas de longitud $L/2$ y masa $m/2$.
Como las dos varillas son iguales el centro de masa será el punto medio del segmento que une los centro de masas de las dos partes (Ver Figura \ref{fig:codo}).
La distancia entre el eje y el centro de masa vale entonces
%
\begin{equation}
D = \frac{L}{4}\cos(30^\circ) = \frac{\sqrt{3}L}{8}.
\end{equation}
%
El momento de inercia se puede calcular como la suma de los momentos de inercias de las dos mitades, es decir dos varillas de longitud $L/2$ y masa $m/2$ que giran al rededor de un extremo.
Sabiendo que el momento de inercia con respeto a un extremo de una barra de longitud $l$ y masa $M$ es $I_{\rm barra}=1/3Ml^2$, obtenemos:
%
\begin{equation}
I = 2\frac{1}{3} \frac{m}{2}\left(\frac{L}{2}\right)^2= \frac{1}{12}mL^2.
\end{equation}
%
Podemos entonces calcular la frecuencia angular usando \eqref{eq:frequencia_angular_pendulo_fisico}:
%
\begin{equation}
\omega_0 = \sqrt{\frac{mgD}{I}} =  \sqrt{\frac{3\sqrt{3}g}{2L}}.
\end{equation}
%
Usando la relación entre frecuencia angular y periodo, obtenemos
%
\begin{equation}
T=\frac{2\pi}{\omega_0}=2\pi \sqrt{\frac{2L}{3\sqrt{3}g}}.
\end{equation}
%
\subsection{El péndulo de torsión}
%
Un péndulo de torsión es constituido por un cuerpo con momento de inercia $I$ vinculado a girar al rededor de un eje fijo y conectado a un muelle de torsión que cuando el sistema gira de un ángulo $\theta$ proporciona un momento proporcional al ángulo de desplazamiento  
%
\begin{equation}
M = -K\theta.
\end{equation}
%
La ecuación del movimiento es
%
\begin{equation}\label{eq:pendulo_torsion}
I \ddot{\theta} = -K\theta,
\end{equation}
%
que es la ecuación de un oscilador armónico simple con frecuencia angular
%
\begin{equation}
\omega_0=\sqrt{\frac{K}{I}}.
\end{equation}
%




\section{Pequeñas oscilaciones al rededor de un punto de equilibrio}  
%
En esta sección demostraremos que, bajo condiciones muy genéricas, el movimiento al rededor de una posición de equilibrio estable de un sistema mecánico unidimensional (cuyo estado puede ser descrito con una sola coordenada, por ejemplo un desplazamiento lineal o angular) es un movimiento armónico simple si la amplitud de las oscilaciones es suficientemente pequeña. 
  
Esto explica porque el movimiento armónico simple es mucho más importante que cualquier otro tipo de movimiento oscilatorio ya que (casi) todos los movimientos oscilatorios se reducen a un movimiento armónico simple en el caso de oscilaciones de pequeña amplitud que es el caso más deseado normalmente en ingeniería, especialmente cuando hacemos referencia a las vibraciones de un sistema mecánico. 
%
\subsection{La serie de Taylor}
%
Para esta demostración necesitaremos una herramienta matemática conocida como serie de Taylor.
La serie de Taylor permite aproximar una función $f(x)$ (suficientemente suave), dentro de un intervalo de valores de la variable independiente $x$ al rededor de un punto central $x_0$, usando potencias crecientes de $x-x_0$.

Los coeficientes de estos polinomios se calculan a partir de las derivadas de $f(x)$ calculadas en el punto $x=x_0$, según la formula 
%
\begin{equation}
f(x) = \sum_{n=0}^\infty \frac{1}{n!} f^{(n)}(x_0) (x-x_0)^n= f(x_0) + f'(x_0) (x-x_0) + \frac{1}{2} f''(x_0) (x-x_0)^2 + \dots .
\end{equation}
%
A partir de esta formula que es exacta (hasta una distancia máxima de $x_0$ llamada radio de convergencia) se pueden construir aproximaciones cortando la suma una vez llegado a cierto $n=n_{\rm max}$.
%
\begin{figure}[hbt]
\centering
\includegraphics[scale=.9]{chapters/oscilaciones/figures/2/taylor.pdf}
\caption{
Aproximación en serie de Taylor de la función exponencial $y=e^x$.
La línea negra representa la función exacta, mientras las líneas coloreadas representan la expansión de Taylor al rededor de $x=0$ de orden $0$ (azul), $1$ (naranja), $2$ (verde), y $3$ (rojo).
}
\label{fig:taylor}
\end{figure}
% 
La aproximación puede ser mejorada incluyendo más términos (aumentando $n_{\rm max}$) como se puede ver en Figura \ref{fig:taylor} dónde podemos ver la comparación entre la función exponencial $e^x$ y su aproximaciones en serie de Taylor de orden $0$ (constante), $1$ (lineal), $2$ (parabólica) y $3$ (cúbica). 

Fijado cierto $n_{\rm max}$ la aproximación en serie de Taylor es mejor cuanto más cerca esté el punto $x$ al punto $x_0$. 

En la práctica necesitaremos la aproximación lineal (orden uno) para aproximar las fuerzas y la aproximación cuadrática (orden dos) para las energías.
%
\subsection{Aproximación en serie de Taylor de fuerza y energía potencial cerca de un punto de equilibrio}
\label{sec:taylor_equilibrio}
%
Imaginemos un sistema mecánico descrito por una coordenada $x$, por ejemplo un sistema masa muelle como el de Figura \ref{fig:masa_muelle}, donde pero la fuerza depende de la posición $x$ según una función conocida $F(x)$.
Un ejemplo sencillo es el mismo sistema masa-muelle con un muelle non ideal, es decir que no respeta la simple ley de Hook.
La ecuación de movimiento de este sistema será 
%
\begin{equation}
m \ddot{x} = F(x),
\end{equation}
%
en general esta ecuación tiene que ser resuelta numéricamente para encontrar la trayectoria $x(t)$.
Consideremos ahora el caso en que el sistema tenga una posición de equilibrio estable en el punto $x=x_0$.
Podemos aproximar la ecuación del movimiento, siempre que esto se desarrolle cerca de $x_0$, expandiendo $F(x)$ en serie de Taylor hasta el primer orden al rededor del punto de equilibrio $x_0$   
%
\begin{equation}
F(x) \approx F(x_0) + F'(x_0) (x-x_0) =  F'(x_0) (x-x_0) = -k_{\rm eq} (x-x_0).
\end{equation}
%
Notamos que el termino de orden cero (constante) de la serie de Taylor es cero porque la fuerza vale cero en la posición de equilibrio.
Como además la posición de equilibrio $x_0$ es una posición de equilibrio estable la pendiente de la fuerza en $x_0$ tiene que ser negativa.
Podemos entonces definir el numero {\it positivo}
%
\begin{equation}
k_{\rm eq} = -F'(x_0),
\end{equation}
%
que jugará el papel de constante elástica equivalente del sistema.
La ecuación de movimiento aproximada del sistema es
%
\begin{equation}
m \ddot{x} = -k_{\rm eq} (x-x_0).
\end{equation}
%
Haciendo un cambio de coordenadas a $\Delta x = x-x_0$, y observando que $\ddot{\Delta x} =d^2(x-x_0)/dt^2 = \ddot{x}$, obtenemos
%
\begin{equation}
m \ddot{\Delta x} = -k_{\rm eq} \Delta x,
\end{equation}
%
que es la ecuación de un oscilador armónico con frecuencia angular
%
\begin{equation}
\omega_0  = \sqrt{\frac{k_{\rm eq}}{m}}.
\end{equation}
%
A la fuerza $F(x)$ podemos asociar una energía potencial $U(x) = -\int F(x) dx$ de manera que
%
\begin{equation}
F(x) = -\frac{dU(x)}{dx}.
\end{equation}
%
La aproximación al segundo orden en serie de Taylor de la energía potencial es 
%
\begin{equation}
U(x) \approx U(x_0) + U'(x_0) (x-x_0)+ \frac{1}{2} U''(x_0) (x-x_0)^2=   U(x_0)+ \frac{1}{2} U''(x_0) (x-x_0)^2 = U(x_0)+ \frac{1}{2} k_{\rm eq} (x-x_0)^2,
\end{equation}
%
donde hemos usado que $U'(x_0)=-F(x_0)=0$ y 
%
\begin{equation}
U''(x_0)=-F'(x_0) = k_{\rm eq}.
\end{equation}
%
La energía potencial cerca del punto de equilibrio es (a parte una constante irrelevante $U(x_0)$) una parábola con concavidad positiva (hacia arriba) y mínimo en el punto de equilibrio, exactamente como la energía potencial de un muelle ideal.

Hemos demostrado que cuando se puede aproximar la fuerza al primer orden en serie de Taylor, al rededor de una posición de equilibrio estable, las pequeñas oscilaciones del sistema serán un movimiento armónico simple. 

Existen pero algunas excepciones (de limitada relevancia práctica) que pueden producirse por dos razones:
% 
\begin{itemize}
\item La fuerza no se puede aproximar en serie de Taylor cerca del punto de equilibrio al no ser una función regular.
Un ejemplo de este tipo es el sistema analizado en el Problema 17 (doble plano inclinado).
\item La fuerza se puede representar en serie de Taylor pero el coeficiente del primer orden de la expansión al rededor del punto de equilibrio es cero: $k_{\rm eq} =-F'(x_0)=0$.
En este caso (por ejemplo $F(x) = -ax^3$) las pequeñas oscilaciones del sistema no serán armónicas.
Un ejemplo de este tipo es analizado en el Problema 21 (doble muelle).
\end{itemize}
%
\subsection{Ejemplo numérico}
%
Para aclarar el procedimiento general vamos a resolver un ejemplo numérico.
Sea la energía de un sistema en función de $x$ dada por la función
%
\begin{equation}\label{eq:potential_ejemplo}
U(x) = x^3-5x+1,
\end{equation}
%
y representada en Figura \ref{fig:approx}.

La fuerza correspondiente es (gráfica en Figura \ref{fig:approx})
%
\begin{equation}\label{eq:force_ejemplo}
F(x) =-\frac{dU(x)}{dx} =-3x^2+5.
\end{equation}
%
\begin{figure}[hbt]
\centering
\includegraphics[scale=.9]{chapters/oscilaciones/figures/2/potential_approx.pdf}
\includegraphics[scale=.9]{chapters/oscilaciones/figures/2/force_approx.pdf}
\caption{
Izquierda: Energía potencial en el ejemplo numérico \eqref{eq:potential_ejemplo}.
La línea discontinua es la aproximación al segundo orden cerca de la posición de equilibrio estable.
El punto marca la posición de equilibrio estable, el triangulo la posición de equilibrio inestable.
Derecha: Fuerza en el ejemplo numérico \eqref{eq:force_ejemplo}.
La línea discontinua es la aproximación al primer orden cerca de la posición de equilibrio estable.
El punto marca la posición de equilibrio estable, el triangulo la posición de equilibrio inestable.
}
\label{fig:approx}
\end{figure}
%
Podemos encontrar las posiciones de equilibrio buscando los puntos donde la fuerza vale cero.
Encontramos dos posiciones de equilibrio $x_1$ y $x_2$:
%
\begin{equation}
x_{1/2} = \pm \sqrt{\frac{5}{3}}.
\end{equation}
%
La derivada de la fuerza es
%
\begin{equation}
F'(x) = -U''(x) = -6x.
\end{equation}
%
Esta cantidad nos permite diferenciar entre posiciones de equilibrio estables y inestables.
En las posiciones estables $F'(x)<0$.
La única posición estable será entonces $x_1=\sqrt{5/3}$.
Podemos ahora calcular la constante elástica equivalente evaluando la derivada de la fuerza en la posición de equilibrio
%
\begin{equation}
k_{\rm eq} =-F'(x_1)= 6 x_1 = 6  \sqrt{\frac{5}{3}} =\sqrt{60}.
\end{equation}
%
La frecuencia angular de las pequeñas oscilaciones de una masa $m$ en este potencial cerca de la posición de equilibrio $x_1$ será entonces
%
\begin{equation}
\omega_0 = \sqrt{\frac{\sqrt{60}}{m}}.
\end{equation}
%
\subsection{Aproximación en serie de Taylor para los movimientos oscilatorios angulares}
Toda la teoría desarrollada para el movimiento de traslación al rededor de una posición de equilibrio tiene un equivalente inmediato en las oscilaciones angulares.
Para un sistema que gira al rededor de un eje fijo la segunda ley de Newton para las rotaciones es
%
\begin{equation}
I\ddot{\theta} = M(\theta)
\end{equation}
%
donde $I$ es el momento de inercia respecto del eje, $\theta$ es el ángulo de rotación y $M$ el momento total de las fuerzas que actúan sobre el sistema.
En una posición de equilibrio de rotación $\theta_0$ el momento total vale cero.
Expandiendo en serie de Taylor el momento hasta el primer orden obtenemos
%
\begin{equation}
M(\theta)=M(\theta_0+\Delta \theta)\approx M(\theta_0) +  M'(\theta_0)\Delta \theta=-K_{\rm eq} \Delta \theta
\end{equation}
% 
donde, siguiendo los mismos pasos que en el caso de la rotación hemos definido una constante de torsión equivalente
%
\begin{equation}
K_{\rm eq}=- M'(\theta_0)
\end{equation}
%
que, en una posición de equilibrio estable es un numero positivo.
Recordando que $\ddot{\Delta \theta}=\ddot{\theta}$ obtenemos una ecuación aproximada de movimiento:
%
\begin{equation}
I\ddot{\Delta \theta} =-K_{\rm eq} \Delta\theta.
\end{equation}
%
Esta ecuación es equivalente a la ecuación de un péndulo de torsión \eqref{eq:pendulo_torsion} con una constante de torsión equivalente $K_{\rm eq}$.
Por lo tanto su frecuencia angular natural de oscilación es
%
\begin{equation}
\omega_0 = \sqrt{\frac{K_{\rm eq}}{I}}.
\end{equation}
% 
%
\section{Ejemplos de problemas de pequeñas oscilaciones}
%
En este capítulo hemos discutido varios sistemas que se comportan {\it aproximadamente} como un oscilador armónico cuando la amplitud de sus oscilaciones es pequeña y hemos demostrado que prácticamente cualquier sistema (salvo las excepciones evidenciadas en Sec.~\ref{sec:taylor_equilibrio}) que tenga una posición de equilibrio estable, hace oscilaciones armónicas al rededor de esta posición de equilibrio si la amplitud del desplazamiento es suficientemente pequeña. Además hemos desarrollado una receta para calcular la frecuencia natural de estas oscilaciones basada en la expansión de Taylor al primer orden de la fuerza (o, equivalentemente,  al segundo orden en la energía).
En el ejemplo numérico discutido en la sección anterior hemos empezado con un potencial conocido exactamente, y encontrando su aproximación de Taylor al segundo orden (primer orden en la fuerza) cerca de la posición de equilibrio estable.

En principio este método nos permite calcular la frecuencia natural de oscilación de cualquier sistema mecánico con un grado de libertad: siempre podemos calcular la expresión exacta de la fuerza (o del momento en el caso de las rotaciones) y usar la serie de Taylor al final del calculo.

En práctica este método, a pesar de ser totalmente correcto, resulta ineficiente a la hora de hacer los cálculos en muchas situaciones prácticas. Para resolver más rápidamente los problemas de pequeñas oscilaciones (y limitar las posibilidades de error) es mejor hacer todas las aproximaciones posibles {\it en cuanto se presente la posibilidad} y no esperar hasta al final.

Explicaremos este concepto usando el siguiente problema que es el prototipo de una clase muy importante de problemas en que tenemos que encontrar la frecuencia natural de oscilación de un sistema mecánico asignado.
%
\subsection{Problema}
%
Una barra de masa $m=20~{\rm kg}$ y de longitud $2L=4~{\rm m}$ está articulada por su punto medio $O$ de forma que puede girar alrededor de $O$ sin ningún tipo de fricción. Un extremo de la misma barra está atado por una muelle de constante $k=60~{\rm N/m}$ al suelo, estando el conjunto en equilibrio cuando la muelle es perpendicular a la barra tal como se indica en la figura \ref{fig:barra}.
%
\begin{figure}[htb]
\centering
\includegraphics[scale=1]{chapters/oscilaciones/figures/2/barra.pdf}
\caption{Esquema del sistema estudiado en el problema.}
\label{fig:barra}
\end{figure}
%

a) Encuentra el periodo $T$ del movimiento.

b) ¿Cuánto valdría el periodo $T'$ de las oscilaciones de esta misma barra si ahora le añadiéramos otro muelle de constante $k'=40~{\rm N/m}$ en el otro extremo de manera que la barra continue en equilibrio horizontal?
%
\subsection{Resolución usando la segunda ley de Newton}
%
La segunda ley de Newton para las rotaciones es
%
\begin{equation}
I \ddot{\theta} = M_{\rm tot}(\theta),
\end{equation}
%
donde el momento de inercia $I$ de una barra de masa $m$ y longitud $2L$ es
%
\begin{equation}
I=\frac{1}{12} m (2L)^2 = \frac{1}{3}mL^2.
\end{equation}
%
y $M_{\rm tot}$ es el momento total de las fuerzas que actúan sobre la barra.
La única fuerza que produce momento sobre la barra es la fuerza del muelle (el peso y la normal del eje de rotación están aplicados en el centro de rotación). Necesitamos entonces calcular la fuerza producida por el muelle cuando la barra está inclinada de un ángulo $\theta$ pequeño y el momento correspondiente.

Cuando la barra se inclina de un ángulo $\theta$ el extremo donde está atado el muelle se mueve a lo largo de una circunferencia de radio $L$ desplazándose de una cantidad 
\begin{equation}
\Delta y = L\sin\theta
\end{equation}
en vertical
y de una cantidad 
\begin{equation}
\Delta x = L(\cos \theta-1)
\end{equation}
en horizontal.
Si el ángulo es pequeño 
\begin{equation}
\Delta y = L\sin\theta\approx L\theta
\end{equation}
\begin{equation}
\Delta x = L(\cos \theta-1)\approx-L\frac{1}{2}\theta^2\approx 0
\end{equation}
porque para ángulos pequeños $\theta^2$ es despreciable comparado con $\theta$.
El punto dónde está atado el muelle se mueve entonces en vertical de una cantidad $\Delta y \approx L\theta$ mientras que su movimiento horizontal es despreciable.
Esta es una característica común a todos los problemas de pequeñas oscilaciones: el movimiento de cada punto se puede aproximar con el movimiento en dirección tangente a la trayectoria en el punto de equilibrio (vertical en este caso) y se puede despreciar el movimiento en la dirección perpendicular.

En esta aproximación el muelle se estira de $\Delta y$ sin inclinarse lateralmente.
La fuerza del muelle es dada por la ley de Hook:
\begin{equation}
F=k\Delta y = kL\theta.
\end{equation}
Como no hay desplazamiento lateral el momento es el momento de una fuerza vertical aplicada a distancia $L$ del centro de rotación. Su módulo vale
\begin{equation}
|M| = kL^2 |\theta|.
\end{equation}
Si $\theta$ es positivo el  momento empuja hacia ángulos negativos (y al revés). 
El valor del momento (incluido el signo) es entonces:
\begin{equation}
M = -kL^2\theta.
\end{equation}
Sustituyendo en la ecuación de Newton obtenemos
\begin{equation}
I \ddot{\theta} = -kL^2\theta,
\end{equation}
que es la ecuación de un oscilador armónico simple con frecuencia angular
\begin{equation}
\omega_0 = \sqrt{\frac{kL^2}{I}}=\sqrt{\frac{3k}{m}}.
\end{equation}
El período es simplemente 
\begin{equation}
T =\frac{2\pi}{\omega_0} =2\pi \sqrt{\frac{m}{3k}}.
\end{equation}
La pregunta (b) se deja como ejercicio para el lector.