import os 
import sys
if len(sys.argv) == 2:
    os.chdir(sys.argv[-1])
cwd = os.getcwd()
for i in range(20):
    path = cwd+'\{}'.format(i)
    if os.path.isdir(path):
        for f in os.scandir(path):
            if f.path[-4:] == '.tex':
                print('Processing    ' + f.path)
                os.chdir(path)
                os.system('pdflatex {} -quiet'.format(f.path))
            if f.path[-6:] == '.ipynb':
                print('Processing    ' + f.path)
                os.chdir(path)
                os.system('jupyter execute {}'.format(f.path))
